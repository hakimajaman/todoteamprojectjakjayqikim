const router = require('express').Router();
const taskController = require('../../controller/taskController')

router.post('/create', taskController.createTodo)
router.put('/update/:id', taskController.updateTodo)
router.delete('/delete/:id', taskController.deleteTodo)
router.get('/index', taskController.indexTodo)
router.get('/index/:name', taskController.getbyname);

module.exports = router;
