var express = require('express');
var router = express.Router();

var usersRoute = require('./v1/users')
var taskRouter = require('./v1/taskRouter')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/users', usersRoute)
router.use('/task', taskRouter)

module.exports = router;
