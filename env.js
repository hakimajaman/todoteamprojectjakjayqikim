let conf;
let cloudinaryconfig = require('./config/cloudinary')
if (process.env.NODE_ENV === 'dev') {
    conf = {
        uri: process.env.DBtodomongo ||  'mongodb+srv://user1:Password123@cluster0-jkupp.mongodb.net/test?retryWrites=true&w=majority'
            && cloudinaryconfig.cloudinaryConfig, // 'mongodb://127.0.0.1:27017/todomongo',
        dom: 'http://localhost:3000'
    };
} else if (process.env.NODE_ENV === 'test') {
    conf = {
        uri: process.env.DBtodomongo ||  'mongodb+srv://user1:Password123@cluster0-jkupp.mongodb.net/test?retryWrites=true&w=majority'
        && cloudinaryconfig.cloudinaryConfig, // 'mongodb://127.0.0.1:27017/todomongo',
        dom: 'http://localhost:3000'
    };
}
module.exports = {
    MONGODB_URI: conf.uri,
    DOMAIN: conf.dom
};
