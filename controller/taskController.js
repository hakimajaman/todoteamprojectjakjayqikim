const Tasks = require('../models/task');
const {sucRes, failRes} = require('../helper/resFormat.js');

function createTodo(req, res){
    const postTodo = new Tasks({
        name: req.body.name,
        date: req.body.date,
        note: req.body.note,
        priority: req.body.priority,
        status: req.body.status
    });
    postTodo.save((err, data) => {
        if(err){
            console.log(err)
        }
        res.json("success" + data);
        console.log(data)
    });
}

function updateTodo (req, res){
    Tasks.findByIdAndUpdate(
        req.params.id, {$set: req.body
        }, (err, data) => {
            if (err) return res.status(404).json({ message: "error", err});
            res.status(200).json(sucRes(req.body, "Entry Update Success"));
        }
    )
}

function deleteTodo (req, res){
    Tasks.findByIdAndDelete(
        req.params.id
        , (err, data) => {
            if (err) return res.status(404).json(failRes("ID Not Found"));
            res.status(201).json(sucRes(data, "Entry Delete Success"));
        }
    )
}

function showTodo (req, res){
    Tasks.findById(
        req.params.id, (err, data)=> {
            if (err) return res.status(404).json(failRes("ID not found"));
            res.status(201).json(sucRes(data, "An Entry Show Success"));
        }
    )
}

function getbyname (req, res) {
    Tasks.find({name: req.params.name}).exec()
        .then((data) => {
            res.status(201).send(data)
        })
        .catch((err) => {
            res.send(err)
        })
}

function indexTodo (req, res){
    Tasks.find({
    }, (err, data) => {
        if (err) return res.status(404).json(failRes("Entry Faiil to Display"));
        res.status(200).json(sucRes(data, "Displaying All Entry Success"));
    })
}

function markTodo(req, res){
}

module.exports = {
    createTodo,
    deleteTodo,
    updateTodo,
    showTodo,
    indexTodo,
    getbyname
};
