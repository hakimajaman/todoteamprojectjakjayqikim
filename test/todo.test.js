process.env.NODE_ENV = 'test';
const server = require ('../app');
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const Task = require('../models/task.js');
const expect = chai.expect

chai.use(chaiHttp);

describe('/POST to todo', () => {
    it('it should post a new todo list', done => {
        chai.request(server)
            .post('/api/task/create/')
            .send({
                name: 'TeamProject',
                date: 27-08-2019,
                note: 'Team project 500 words',
                priority: 'high',
                status: true
            })
            .end(function(err, res) {
                if(err){
                    console.log(err)
                }
                expect(res).to.have.status(200);
                done();
            })
    })
})

describe('/GET root path', () => {
    it ("should return true in root path", (done) => {
        chai.request(server).get('/').end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.an('object');
            res.body.should.have.property('success').equal(true);
            res.body.should.have.property('messages').equal("Welcome to ToDo API");
            done();
        })
    })
})

describe('/GET all task', () => {
    it ("should  return true in root path", (done) => {
        chai.request(server).get('/api/task/index').end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.an('object')
            done();
        })
    })
})

describe('/find todo by name', () => {
    it('it should find a todo by name', done => {
        let name = "TeamProject"
        chai.request(server)
            .get('/api/task/index/' + name)
            .end(function(err, res) {
                if(err){
                    console.log(err)
                }
                expect(res).to.have.status(201);
                done();
            })
    })
})

describe('/DELETE todo by id', () => {
    it ("should delete one task based on ID", (done) => {
        chai.request(server).delete('/api/task/delete/5d62395aeac1ea0e6a005cc5').end((err, res) => { 
            res.should.have.status(201);
            res.body.should.be.an('object')
            done();
        })
    })
})

describe('/PUT todo by id', () => {
    it ("should update one task based on ID", (done) => {
        chai.request(server).put('/api/task/update/5d6504698c8bd15576915c16')
            .send({
                name: 'TeamProjectss',
                date: 2019-08-27,
                note: 'Team project 500 words',
                priority: 'high',
                status: true
            })
            .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.an('object')
            done();
        })
    })
})

