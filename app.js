var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');

var app = express();

//added connection
var mongoose = require('mongoose')
var env = require('./env')
var dotenv = require('dotenv')

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', indexRouter);

app.get('/', (req, res) => {
  res.status(200).json({
      success: true,
      messages: "Welcome to ToDo API"
  })
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

mongoose.connect(env.MONGODB_URI, {
  useNewUrlParser: true,
  useFindAndModify: false
}, (err) => {
  if (err) {
    return console.log(err);
  }
  console.log('MongoDB Connected');
});
mongoose.Promise = global.Promise;

module.exports = app;
